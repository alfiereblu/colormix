
import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var colorView: UIView!
        
    @IBOutlet weak var textViewUp: UITextView!
    @IBOutlet weak var textViewDown: UITextField!
        
    @IBOutlet weak var textSize: UILabel!
    
    @IBOutlet weak var stepper: UIStepper!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var redSwitch: UISwitch!
    @IBOutlet weak var greenSwitch: UISwitch!
    @IBOutlet weak var blueSwitch: UISwitch!
        
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    @IBOutlet weak var Reset: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorView.layer.borderWidth = 3
        colorView.layer.cornerRadius = 20
        colorView.layer.borderColor = UIColor.black.cgColor
        
        redSlider.isEnabled = false
        greenSlider.isEnabled = false
        blueSlider.isEnabled = false
                    
        textViewDown.layer.borderWidth = 1
        textViewDown.layer.cornerRadius = 5
        textViewDown.layer.borderColor = UIColor.gray.cgColor
        
        stepper.layer.cornerRadius = 15
        
        textSize.text = "\(textViewUp.font?.pointSize ?? 10.0)"
        stepper.value = Double(textViewUp.font!.pointSize)
        
        textViewUp.textColor = .white
        textViewDown.delegate = self
        
        segmentedControl.layer.cornerRadius = 15

//        Aggiungere un ulteriore segmento
//        segmentedControl.insertSegment(withTitle: "Lorenzo", at: 2, animated: true)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }

   @IBAction func switchChanged(_ sender: UISwitch) {
    
        updateColor()
        updateControls()
    
    }
    
    func updateColor(){
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        
        if redSwitch.isOn {
            red = CGFloat(redSlider.value)
        }
        if greenSwitch.isOn {
            green = CGFloat(greenSlider.value)
        }
        if blueSwitch.isOn {
            blue = CGFloat(blueSlider.value)
        }
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        
        colorView.backgroundColor = color

 
  }
    
    @IBAction func sliderChanged(_ sender: Any) {
        updateColor()
    }
    
    
    @IBAction func reset(_ sender: Any) {
        redSlider.value = 1
        greenSlider.value = 1
        blueSlider.value = 1
        redSwitch.isOn = false
        greenSwitch.isOn = false
        blueSwitch.isOn = false
        updateControls()
        colorView.backgroundColor = .black

        
    }
    
    func updateControls(){
        redSlider.isEnabled = redSwitch.isOn
        greenSlider.isEnabled = greenSwitch.isOn
        blueSlider.isEnabled = blueSwitch.isOn
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func stepperChanged(_ sender: UIStepper) {
            textSize.text = "\(stepper.value)"
            textViewUp.font = .systemFont(ofSize: CGFloat(stepper.value))
    }
    
    @IBAction func segmentedControlAction(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 1 {

//            updateSliderConstrains()

            redSwitch.isHidden = true
            greenSwitch.isHidden = true
            blueSwitch.isHidden = true
            

            
//            redSlider.updateConstraints() {
//                redSlider.constraints = NSLayoutConstraint(item: self, attribute: nil, relatedBy: nil, toItem: self.view, attribute: nil, multiplier: 1.0, constant: 8.0)
            
            redSlider.isEnabled = true
            greenSlider.isEnabled = true
            blueSlider.isEnabled = true
            
            redSlider.tintColor = .systemGray
            greenSlider.tintColor = .systemGray
            blueSlider.tintColor = .systemGray
            
        }
            
        else {
            redSwitch.isHidden = false
            greenSwitch.isHidden = false
            blueSwitch.isHidden = false
            
            updateControls()

            redSlider.tintColor = .systemRed
            greenSlider.tintColor = .systemGreen
            blueSlider.tintColor = .systemBlue
        }
    }
    
//    func updateSliderConstrains() {
//            view.addSubview(redSlider)
//            redSlider.translatesAutoresizingMaskIntoConstraints = false
//            redSlider.leadingAnchor.constraint(equalTo: redSwitch.leadingAnchor).isActive = true
//            redSlider.trailingAnchor.constraint(equalTo: redSlider.trailingAnchor).isActive = true
//            redSlider.topAnchor.constraint(equalTo: redSwitch.topAnchor).isActive = true
//            redSlider.bottomAnchor.constraint(equalTo: redSwitch.bottomAnchor).isActive = true
//
//    }
    

//    Quando inizia qualcosa nell'oggetto collegato
    @IBAction func editidingDidBegin(_ sender: Any) {
        print("Ciaoooo")
    }
//    Quando cambia il valore dell'oggetto collegato
    @IBAction func valueChanged(_ sender: Any) {
        print("valore cambiato")
    }
//    Quando termina un evento qualcosa nell'oggetto collegato
    @IBAction func editingDidEnd(_ sender: Any) {
        print("editing ended")
        textViewUp.text = textViewDown.text
        textViewDown.resignFirstResponder ()
        
        
//        Cambiare size della label
//        var testo = UILabel()
//        testo.font.withSize(<#T##fontSize: CGFloat##CGFloat#>)
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
}


